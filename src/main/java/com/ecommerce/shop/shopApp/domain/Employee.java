package com.ecommerce.shop.shopApp.domain;

import com.ecommerce.shop.shopApp.typeEnum.Role;

import javax.persistence.*;
import java.util.Date;

/**
 * @author : Mickey Ndiweni - 24/11/2018
 */

@Entity(name = "employee")
public class Employee {

    private Integer id;
    private String firstName;
    private String surname;
    private Role role;
    private Date dateOfBirth;

    private Employee(Builder builder) {
        this.id = builder.id;
        this.firstName = builder.firstName;
        this.surname = builder.surname;
        this.role = builder.role;
        this.dateOfBirth = builder.dateOfBirth;
    }

    public Employee(){
    }

    @Id
    @GeneratedValue
    @Column(name = "id")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "first_name")
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @Column(name = "surname")
    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    @Column(name = "work_role")
    @Enumerated(EnumType.STRING)
    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    @Column(name = "dob")
    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public static class Builder {

        private Integer id;
        private String firstName;
        private String surname;
        private Role role;
        private Date dateOfBirth;

        public Builder setId(Integer id) {
            this.id = id;
            return this;
        }

        public Builder setFirstName(String firstName) {
            this.firstName = firstName;
            return this;
        }

        public Builder setSurname(String surname) {
            this.surname = surname;
            return this;
        }

        public Builder setRole(Role role) {
            this.role = role;
            return this;
        }

        public Builder setDateOfBirth(Date dateOfBirth) {
            this.dateOfBirth = dateOfBirth;
            return this;
        }

        public Employee build() {
            return new Employee(this);
        }
    }
}
