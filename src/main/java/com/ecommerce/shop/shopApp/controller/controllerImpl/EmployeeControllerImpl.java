package com.ecommerce.shop.shopApp.controller.controllerImpl;

import com.ecommerce.shop.shopApp.controller.EmployeeController;
import com.ecommerce.shop.shopApp.domain.Employee;
import com.ecommerce.shop.shopApp.exception.NoRecordException;
import com.ecommerce.shop.shopApp.service.EmployeeManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

/**
 *
 * @author : Mickey Ndiweni - 24/11/2018.
 */

@RestController
@RequestMapping("/employees")
public class EmployeeControllerImpl implements EmployeeController {

    @Autowired
    private EmployeeManager employeeManager;

    @GetMapping("/{id}")
    public Employee getEmployee(@PathVariable final Integer id) {

        Employee emp = null;
        try {
            emp = employeeManager.getEmployee(id);
        } catch (NoRecordException e) {
            e.printStackTrace();
    }
        return emp;
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public Collection<Employee> getAllEmployees() {

        return employeeManager.getAllEmployees();
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public Boolean saveEmployee(@RequestBody final Employee employee) {

        return employeeManager.saveEmployee(employee);
    }

    @DeleteMapping("/remove/{employeeId}")
    public Boolean deleteEmployee(@PathVariable final Integer employeeId) {

        Employee empToDelete = null;
        try {
            empToDelete = employeeManager.getEmployee(employeeId);
        } catch (NoRecordException e) {
            e.printStackTrace();
        }
        return employeeManager.deleteEmployee(empToDelete);
    }
}
