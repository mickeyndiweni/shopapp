package com.ecommerce.shop.shopApp.controller;

import com.ecommerce.shop.shopApp.domain.Employee;

import java.util.Collection;

public interface EmployeeController {

    Employee getEmployee(Integer employeeId);

    Collection<Employee> getAllEmployees();

    Boolean saveEmployee(Employee employee);

    Boolean deleteEmployee(Integer employeeId);

}
