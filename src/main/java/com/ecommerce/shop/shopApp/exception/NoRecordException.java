package com.ecommerce.shop.shopApp.exception;

public class NoRecordException extends Throwable {

    public NoRecordException() {
    }

    public NoRecordException(String message) {
        super(message);
    }

    public NoRecordException(String message, Throwable cause) {
        super(message, cause);
    }

    public NoRecordException(Throwable cause) {
        super(cause);
    }

    public NoRecordException(String message,
                             Throwable cause,
                             boolean enableSuppression,
                             boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
