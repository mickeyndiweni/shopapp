package com.ecommerce.shop.shopApp.typeEnum;

public enum  Role {

    REGIONALMANAGER,
    SHIFTMANAGER,
    SUPERVISOR,
    SALESASSISTANT,
    CLEANER,
    INTERN,
    CONTRACTOR


}
