package com.ecommerce.shop.shopApp.service.serviceImpl;

import com.ecommerce.shop.shopApp.domain.Employee;
import com.ecommerce.shop.shopApp.exception.NoRecordException;
import com.ecommerce.shop.shopApp.repository.EmployeeRepository;
import com.ecommerce.shop.shopApp.service.EmployeeManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Optional;

/**
 *
 * @author : Mickey Ndiweni - 24/11/2018.
 */
@Service
public class EmployeeManagerImpl implements EmployeeManager {

    @Autowired
    private EmployeeRepository employeeRepository;

    public EmployeeManagerImpl(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    @Override
    public Collection<Employee> getAllEmployees() {
        return employeeRepository.findAll();
    }

    @Override
    public Employee getEmployee(Integer empNo) throws NoRecordException {
        Optional<Employee> employee = employeeRepository.findById(empNo);
        if (!employee.isPresent()) {
            throw new NoRecordException("Record not found!");
        }
        return employee.get();
    }

    @Override
    public Boolean deleteEmployee(Employee employee) {
        employeeRepository.delete(employee);
        Optional<Employee> deletedRecord = employeeRepository.findById(employee.getId());
        return !deletedRecord.isPresent();
    }

    @Override
    public Boolean saveEmployee(Employee employee) {
        employeeRepository.save(employee);
        Optional<Employee> savedRecord = employeeRepository.findById(employee.getId());
        return savedRecord.isPresent();
    }
}
