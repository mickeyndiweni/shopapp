package com.ecommerce.shop.shopApp.service;

import com.ecommerce.shop.shopApp.domain.Employee;
import com.ecommerce.shop.shopApp.exception.NoRecordException;

import java.util.Collection;

public interface EmployeeManager {

    Collection<Employee> getAllEmployees();

    Employee getEmployee(Integer empNo) throws NoRecordException;

    Boolean deleteEmployee(Employee employeeID);

    Boolean saveEmployee(Employee employee);
}
