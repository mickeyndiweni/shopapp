package com.ecommerce.shop.shopApp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;

@SpringBootApplication

@EntityScan( basePackages = {"com.ecommerce.shop.shopApp.domain"} )
public class ShopAppApplication {
    public static void main(String[] args) {
		SpringApplication.run(ShopAppApplication.class, args);
	}
}
